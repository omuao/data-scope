# data-scope

#### Description
基于open-scope重构的数据权限框架2.0。
它和open-scope的区别：
相同点：
提供对各种ORM框架的底层的数据权限支持。
不同点：
1、data-scope相较于open-scope会新增加缓存机制，以减少open-scope频繁获取数据权限的数据的问题。
2、减少AOP切面配置，全面采用@EnableDataScope注解进行自动化配置
3、提供对数据列权限的控制，提供两种方案（一种是显示层数据列的过滤，另一种是对ORM框架底层查询做的减少数据列筛选。）

DataScope相较于OpenScope。它解决了OpenScope在数据权限遇到的一些数据量请求过大导致的性能问题，同时也解决了OpenScope相对比较复杂的配置内容。





#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)