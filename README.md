# data-scope

#### 介绍
基于open-scope重构的数据权限框架2.0。

它和open-scope的区别：

相同点：

提供对各种ORM框架的底层的数据权限支持。

不同点：

1、data-scope相较于open-scope会新增加缓存机制，以减少open-scope频繁获取数据权限的数据的问题。

2、减少AOP切面配置，全面采用@EnableDataScope注解进行自动化配置

3、提供对数据列权限的控制，提供两种方案（一种是显示层数据列的过滤，另一种是对ORM框架底层查询做的减少数据列筛选。）

DataScope相较于OpenScope。它解决了OpenScope在数据权限遇到的一些数据量请求过大导致的性能问题，同时也解决了OpenScope相对比较复杂的配置内容。





#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)